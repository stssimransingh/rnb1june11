function first(a, b, c){
    switch(c){
        case 'add':
            return a + b;
            break;
        case 'sub':
            return a - b;
            break;
        case 'mul':
            return a * b;
            break;
        case 'div':
            return a / b;
            break;
        default:
            return 'ERR';
            break;
    }

}
let a = first(10,20, 'sub');
// add | sub | mul | div 
console.log(a);